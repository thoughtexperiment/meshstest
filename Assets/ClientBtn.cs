﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientBtn : MonoBehaviour, IInputClickHandler
{
    public GameObject SharingClient;
    public GameObject HostBtnObj;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        //Start Spatial Mapping - hide first two buttons and create a mapping complete button

        HostBtnObj.SetActive(false);
       SharingClient.SetActive(true);
       


    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
