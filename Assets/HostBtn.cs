﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HostBtn : MonoBehaviour, IInputClickHandler
{
    public GameObject spatialmap;
    public GameObject Sharing;
    public GameObject ClientbtnObj;
    public GameObject SPTLbtnObj;
   

    public void OnInputClicked(InputClickedEventData eventData)
    {
        //Start Spatial Mapping - hide first two buttons and create a mapping complete button

        spatialmap.SetActive(true);
        Sharing.SetActive(true);
        SPTLbtnObj.SetActive(true);
        ClientbtnObj.SetActive(false);
        this.gameObject.SetActive(false);
        
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
