﻿using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.SpatialMapping;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpatialMappingBtn : MonoBehaviour, IInputClickHandler
{
    public GameObject spatialmap;
    public GameObject RemoteMapping;

   

    public void OnInputClicked(InputClickedEventData eventData)
    {
        //Start Spatial Mapping - hide first two buttons and create a mapping complete button
        RemoteMapping.SetActive(true);
        RemoteMappingManager.Instance.SendMeshesToHoloLens = true;


    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
